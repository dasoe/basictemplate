#lib.contentElement.templateRootPaths.10 = EXT:basictemplate/Resources/Private/Templates/FluidStyledContent/
#lib.contentElement.partialRootPaths.10 = EXT:basictemplate/Resources/Private/Partials/FluidStyledContent/


[global]
page = PAGE

page {
  headerData {
    10 = TEXT
    10.value (
    
<!-- Fav-ICONS -->


<link rel="apple-touch-icon" sizes="180x180" href="/geqcosFavicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/geqcosFavicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/geqcosFavicons/favicon-16x16.png">
<link rel="manifest" href="/geqcosFavicons/site.webmanifest">
<link rel="mask-icon" href="/geqcosFavicons/safari-pinned-tab.svg" color="#1c6767">
<link rel="shortcut icon" href="/geqcosFavicons/favicon.ico">
<meta name="msapplication-TileColor" content="#1c6767">
<meta name="msapplication-config" content="/geqcosFavicons/browserconfig.xml">
<meta name="theme-color" content="#1c6767">


    )
 #   3750 = TEXT
 #   3750 {
 # typolink.parameter.data = TSFE:id
 # typolink.returnLast = url
 # wrap = <link rel="canonical" href="https://geqcos.wmi.badw.de|"  />
 #   }
  }
}

config {
  no_cache = 0
  doctype = xhtml_trans
  renderCharset = utf-8
  htmlTag_langKey = en
  admPanel = 0
  no_cache = 0
  debug = 0
  simulateStaticDocuments = 0
  absRefPrefix = /  
  tx_realurl_enable = 0
  prefixLocalAnchors = all
  sys_language_uid = 0 
  language = en
  locale_all = en_EN
  date_stdWrap.strftime= %d.%m.%y
  time_stdWrap.strftime= %H:%M  
  #tx_extbase.features.skipDefaultArguments = 1  
}


page.includeCSS {
    main = EXT:basictemplate/Resources/Public/Css/Main.css
    queries = EXT:basictemplate/Resources/Public/Css/MediaQueries.css
    yearpicker = EXT:basictemplate/Resources/Public/Css/yearpicker.css
}
page.includeJS {
    jquery = EXT:basictemplate/Resources/Public/Js/jquery-3.4.1.min.js
    velocity = EXT:basictemplate/Resources/Public/Js/velocity.min.js
    yearpicker = EXT:basictemplate/Resources/Public/Js/yearpicker.js
    #jquery-validate = EXT:basictemplate/Resources/Public/Js/jquery.validate.min.js
}

page.includeJSFooter {
    main = EXT:basictemplate/Resources/Public/Js/Main.js
}

page {  
    meta{    
     viewport = width=device-width, initial-scale=1, maximum-scale=1    
     }  
} 

lib.mainmenu = HMENU
lib.mainmenu {
   stdWrap {
     wrap = <div id="mainmenu">|</div>
   }
    
   1 = TMENU
   1 {
    expAll = 1
     wrap = <ul class="menuLevel1">|</ul>   
     NO = 1
     NO {
       allStdWrap.dataWrap = <li class="pageUid{field:uid} norm" > | </li>
       #additionalParams = &no_cache=1
     }
     ACT = 1
     ACT < .NO
     ACT.allStdWrap.dataWrap = <li class="pageUid{field:uid} act"   >|</li>
     CUR = 1
     CUR < .NO
     CUR.allStdWrap.dataWrap = <li class="pageUid{field:uid} cur"   >|</li>
     IFSUB = 1
     IFSUB < .NO
     IFSUB.allStdWrap.dataWrap = <li class="pageUid{field:uid} norm sub"   >|
     CURIFSUB = 1
     CURIFSUB < .NO
     CURIFSUB.allStdWrap.dataWrap = <li class="pageUid{field:uid} cur sub"   >|
     ACTIFSUB = 1
     ACTIFSUB < .NO
     ACTIFSUB.allStdWrap.dataWrap = <li class="pageUid{field:uid} act sub"   >|

   }
   2 = TMENU
   2 {
     expAll = 1
     wrap = <ul class="menuLevel2">|<div class="clear"></div></ul></li>   
     NO = 1
     NO {
       allStdWrap.dataWrap = <li class="pageUid{field:uid} norm" > | </li>
       #additionalParams = &no_cache=1
     }
     ACT = 1
     ACT < .NO
     ACT.allStdWrap.dataWrap = <li class="pageUid{field:uid} act"   >|</li>
     CUR = 1
     CUR < .NO
     CUR.allStdWrap.dataWrap = <li class="pageUid{field:uid} cur"   >|</li>
   }
}
  
lib.pageContent = CONTENT
lib.pageContent {
    value.data = Data
    stdWrap.wrap = <div id="pageContetn">|</div>
    table = tt_content
    select {
        pidInList.data = field:pageId
        orderBy = sorting
    }
}

lib.footerCol1 = CONTENT
lib.footerCol1 {
    value.data = Data
    table = tt_content
    select {
        pidInList = 233
        where = {#colPos}=0
        orderBy = sorting
    }
}

lib.footerCol2 = CONTENT
lib.footerCol2 {
    value.data = Data
    table = tt_content
    select {
        pidInList = 233
        where = {#colPos}=1
        orderBy = sorting
    }
}



lib.nutznavi = HMENU
lib.nutznavi {
   stdWrap {
     wrap = <div id="nutznavi">|</div>
   }
   special = directory
   special.value = 179
   1 = TMENU
   1 {
    expAll = 1
     NO = 1
     NO {
       allStdWrap.dataWrap = <li class="pageUid{field:uid} norm" > | </li>
       #additionalParams = &no_cache=1
     }
     ACT = 1
     ACT < .NO
     ACT.allStdWrap.dataWrap = <li class="pageUid{field:uid} act"   >|</li>
     CUR = 1
     CUR < .NO
     CUR.allStdWrap.dataWrap = <li class="pageUid{field:uid} cur"   >|</li>
     wrap = <ul class="menuLevel1">|</ul>   
     IFSUB = 1
     IFSUB < .NO
     IFSUB.allStdWrap.dataWrap = <li class="pageUid{field:uid} cur"   >|
     ACTIFSUB = 1
     ACTIFSUB < .NO
     ACTIFSUB.allStdWrap.dataWrap = <li class="pageUid{field:uid} act"   >|

   }

}
  
lib.contentRightUp < styles.content.get
lib.contentRightDown < styles.content.get

lib.contentRightUp.select.where = {#colPos}=1
lib.contentRightDown.select.where = {#colPos}=2

lib.contentRightFix < styles.content.get
lib.contentRightFix.select.pidInList = 180


page.10 = FLUIDTEMPLATE
page.10 {
  file = EXT:basictemplate/Resources/Private/Templates/Page/Main.html
    templateRootPaths.10 = EXT:basictemplate/Resources/Private/Templates/
    partialRootPaths.10 = EXT:basictemplate/Resources/Private/Partials/
    layoutRootPaths.10 = EXT:basictemplate/Resources/Private/Layouts/
    variables {
      startpageId = TEXT
      startpageId.value = {$dasoe.basictemplate.startpageId}
      searchpageId = TEXT
      searchpageId.value = {$dasoe.basictemplate.searchpageId}
      content < styles.content.get
      contentRightUp < lib.contentRightUp
      contentRightDown < lib.contentRightDown
      contentRightFix < lib.contentRightFix
      mainmenu < lib.mainmenu
      researchtopics < lib.researchTopics
      researchsubtopics < lib.researchSubTopics
      breadcrumb < lib.breadcrumb
      footerCol1 < lib.footerCol1  
      footerCol2 < lib.footerCol2
      nutznavi < lib.nutznavi
    }    
}
