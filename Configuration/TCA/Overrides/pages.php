<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            'basictemplate',
            'Configuration/TSconfig/Page/BackendLayouts.tsconfig',
            'Backend Layouts geqcos'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            'basictemplate',
            'Configuration/TSconfig/Page/Settings.tsconfig',
            'Settings'
    );       
    
    
});
