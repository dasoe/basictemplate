/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var oUnt;
var oPx;
var env;
var lastEnv;

var darkGreen = "#1c6767";
var lightGreen = "#7ac5bb";

$(document).ready(function () {
    console.log("ready!");

    updateUnits();

    $('#mainmenu .menuLevel1 li').on("mouseover", function (event, data) {
        if (!$(this).hasClass("open")) {
            openSubMenu($(this));
        }

    });
    $('#menuCloseOverlay').on("mouseover", function (event, data) {
        $(".menuLevel2").removeClass("open");
        $(this).css('display', 'none');
    });

    $('svg path#garching').on("mouseover", function (event, data) {
        console.log(svgObject);
    });



//
//
//    let svgObject = document.getElementById('partnerkarte').contentDocument;
//    svgObject.addEventListener("load", function () {
//
//        
        let partners = [ "garching", "juelich", "erlangen", "karlsruhe", "freiburg", "eggenstein", "neubiberg" ];
            console.log( partners.length + 'partners are set..');

        partners.forEach(function callbackFn(element) {
            console.log('setting svg #'+element+'Trigger');
                $('svg #'+element+'Trigger').on("mouseover", function (event, data) {
                    $('svg #'+element).css('fill',darkGreen);
                    $('svg #'+element+"label").css('opacity',1);
                    $('.frame-'+element).css('background-image','url("typo3conf/ext/basictemplate/Resources/Public/Media/circle_darkgreen.svg")');
                    $('.frame-'+element + ' a').css('color','#1C6767');
                    console.log("in");
                });
                $('svg #'+element+'Trigger').on("mouseout", function (event, data) {
                    $('svg #'+element).css('fill',lightGreen);
                    $('svg #'+element+"label").css('opacity',0);
                    $('.frame-'+element).css('background-image','none');
                    $('.frame-'+element + ' a').css('color','#707070');
                    console.log("out");
                 });
                $('svg #'+element+'Trigger').on("click", function (event, data) {
                   // console.log("click!");  
                    window.location.href =  $('.frame-'+element + ' a').attr("href") ;
                });

            $('.frame-'+element).on("mouseover", function (event, data) {
                    $('svg #'+element).css('fill',darkGreen);
                    $('svg #'+element+"label").css('opacity',1);
                    $('.frame-'+element).css('background-image','url("typo3conf/ext/basictemplate/Resources/Public/Media/circle_darkgreen.svg")');
                    $('.frame-'+element + ' a').css('color','#1C6767');
                 });       
                $('.frame-'+element).on("mouseout", function (event, data) {
                    $('svg #'+element).css('fill',lightGreen);
                    $('svg #'+element+"label").css('opacity',0);
                    $('.frame-'+element).css('background-image','none');
                    $('.frame-'+element + ' a').css('color','#707070');
                   console.log("out");
                });           
            
            
        });

//        
//    }, false);


    

    



    // same for outer collapsable containers
    $('.collapsableContent').not($('.assoc .collapsableContent')).each(function (event, data) {
        $(this).data('originalHeight', $(this).height() + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom')));
        $(this).data('orPadTop', parseInt($(this).css('padding-top')));
        console.log($(this).data('originalHeight'));
        // then set height to 0
        $(this).css({
            'height': 0,
            'padding-top': 0,
        });
    });

    // respectice stuff for margin of header
    $('.collapsableHeader').each(function (event, data) {
        $(this).data('orMarBot', parseInt($(this).css('margin-bottom')));
        // then set height to 0
        $(this).css({
            'margin-bottom': 0
        });
    });


    $('.collapsableHeader').on("click", function (event, data) {
        if (!$(this).hasClass("open")) {
            $(this).addClass("open");
            let container = $(this).next('.collapsableContent')
            container.addClass("open");
            container.velocity({
                height: container.data('originalHeight'),
                paddingTop: container.data('orPadTop'),
            }, 650, function () {
                container.css("height", "auto");
            });
            $(this).velocity({
                marginBottom: $(this).data('orMarBot'),
            });

        } else {
            $(this).removeClass("open");
            let container = $(this).next('.collapsableContent')
            container.removeClass("open");
            container.velocity({
                height: 0,
                paddingTop: 0,
            }, 650, function () {
                // ready
            });

            $(this).velocity({
                marginBottom: 0,
            });
        }
    });

    $(window).scroll(function (event) {
        // console.log("Scrolling...");
        // on homepage
//        if ($('#homeWrap').length) {
//            // on scroll: adjust menu.  
//            if ($(document).scrollTop() > 5) {
//                $('#header').removeClass('homeTrans');
//
//            } else {
//                $('#header').addClass('homeTrans');
//            }
//        }

    });


    adjustOnPageLoad();

    // do the same in case window is resized
    $(window).resize(function () {
        adjustAll();
    });




});

function updateUnits() {
    // columns in Content are calculated by flex
    // get size of one Unity ( one quad in grid )
    // using columns calculated by flex as a basis

//    console.log($(window).innerWidth());
//    console.log(1600 / ($(window).innerWidth() * 5));


    $('#oeVars').html($(window).innerWidth());
    if ($(window).innerWidth() > 1024) {
        // Desktop
//        oUnt = $('#wrap').width() / 48;
        oPx = ($(window).innerWidth() / 1600);
        +(1600 / ($(window).innerWidth() * 10));
        if (oPx > 1) {
            oPx = 1;
        }
        env = "desktop";
    } else if ($(window).innerWidth() > 440 && $(window).innerWidth() < 1025) {
        // Tablet
//        oUnt = $('#wrap').width() / 25;
        env = "tablet";
    } else {
        // Mobile
//        oUnt = $('#wrap').width() / 9;
        env = "mobile";
    }

    // override TODO: DAs ist fragwürdig, oder??
//    if (window.innerHeight > window.innerWidth) {
//        // Mobile
//        oUnt = $('#wrap').width() / 9;
//        env = "mobile";
//    }
//    console.log(window.devicePixelRatio);
//
//    console.log('oUnt after updating: ' + oUnt + ' - oPx after updating: ' + oPx);

}


function adjustAll() {
    //console.log("adjust All");
    updateUnits();
    console.log('w: ' + $(window).innerWidth());


    if (env != lastEnv) {
        console.log("############################### (re)setting environment! ###################################");
        lastEnv = env;
    }

    if (env == "desktop") {

        if ($(window).innerWidth() <= 300) {
            $('html').css({
                'font-size': oPx * 16.2,
            });
        }

        // depending on window size: Font size as basis for rem/em calculation
        if ($(window).innerWidth() >= 1300) {
            $('#wrap').css({
                'font-size': oPx * 16.2,
            });
        } else {
            $('#wrap').css({
                'font-size': oPx * 18,
            });
        }


//        $('#footer .footerInnerWrap').css({
//            'padding-left': oPx * 100,
//            'padding-right': oPx * 60,
//        });
//        console.log( 'w: ' + $(window).innerHeight() );
//        console.log($('#innerWrap').css('padding-top'));
//        console.log($('#footer').height());
        let bH = $(window).innerHeight() - parseInt($('#innerWrap').css('padding-top')) - $('#footer').height() - parseInt($('#footer').css('padding-top')) - parseInt($('#footer').css('padding-bottom'));
        $('.broad').css({
            'min-height': bH
        });


    }
}

function adjustOnPageLoad() {

    // select first header
    $('h1').eq(0).addClass('firstHeader');

    let resSubMen = $('#reasearchMenuProvider').html();
    $('#mainmenu .menuLevel1 li.pageUid10').append(resSubMen);
    console.log(resSubMen);
//    if ($('#homeWrap').length) {
//        // on scroll: adjust menu.  
//        if ($(document).scrollTop() > 5) {
//            $('#header').removeClass('homeTrans');
//
//        } else {
//            $('#header').addClass('homeTrans');
//        }
//    }

    adjustAll();

}

function openSubMenu(element) {
    console.log("open sub menu");
    $(".menuLevel2").removeClass("open");
    element.find(".menuLevel2").addClass("open");
    $('#menuCloseOverlay').css("display", 'block');
}

